import argparse
import os
import re
from collections import namedtuple
from typing import NamedTuple

from argument_lib.settings import OUTPUT_FILE, CELL_INIT_FILE

Args = namedtuple('Arguments', ['source_path', 'cell_init_file', 'output_file', 'type_of_file'])
File = namedtuple('File', ['path', 'filename', 'counter', 'extension'])

counter_form = re.compile(r'(.+)_([0-9]+)$')


def create_argument_parser():
    parser = argparse.ArgumentParser(description=(
        'Connect all ODS/XLS files in folder to one XLS file (default=summary.xlsx) according to'
        f' rule "one-file one-row" where columns of rows are given by configuration file (default={CELL_INIT_FILE})'
    ))
    parser.add_argument('source_path', metavar='[path]', help='absolute path where ODS/XLSX files are')
    parser.add_argument('-o', '--output-file', default=OUTPUT_FILE, help='the file where print all gained data')
    parser.add_argument(
        '-c', '--cell-init-file', default=CELL_INIT_FILE,
        help='the file with names of cells, where wanted data are,'
             ' e.g. "A2 B2\\nA3 B3", where A2 and A3 are the (column) names of data, B2 and B3 are the values of data'
    )
    parser.add_argument(
        '-t', '--type-of-file', default='xlsx', choices=['ods', 'xlsx'],
        help='format of source data files and output file, two possibilities from MS Excel or LibreOffice'
    )
    return parser


def validate_args(args: argparse.Namespace) -> NamedTuple:
    """ validates arguments gained on input """
    if not os.path.isabs(args.source_path):
        raise ValueError(f'Parameter {args.source_path} is not full path.')

    cell_init_file: str = get_path(args.cell_init_file, args.source_path)
    if not os.path.isfile(cell_init_file):
        raise ValueError(f'Parameter {cell_init_file} is not file.')

    extension = f'.{args.type_of_file}'
    output_file: str = get_extension(get_path(args.output_file, args.source_path), extension)
    output_file = get_unique_file_name(output_file)

    if not output_file.endswith(extension):
        file_without_path = os.path.split(output_file)[1]
        raise ValueError(f'Type of file ({extension}) disagree with extension of output file ({file_without_path}).')

    return Args(args.source_path, cell_init_file, output_file, extension)


def get_path(file: str, source_path) -> str:
    """ connects path into file in case path is missing """
    if not os.path.isabs(file):
        file = os.path.join(source_path, file)
    return file


def get_extension(file: str, extension: str) -> str:
    """ adds extension in case file does have it """
    return file if os.path.splitext(file)[1] else f'{file}{extension}'


def get_unique_file_name(file: str) -> str:
    """
    when file already exists adding and increase recursively number in suffix of file name (e.g. summary_3.xlsx)
    till file name is unique (=file with such name does not exist yet)
    :returns: unique name with absolute path
    """
    if os.path.isfile(file):
        file = get_unique_file_name(inc_counter(file))
    return file


def inc_counter(file: str) -> str:
    """ increments number of counter in filename, adds counter if counter is missing """
    file: File = split_file(file)
    counter: str = file.counter if file.counter else '1'
    return os.path.join(file.path, f"{file.filename}_{counter}{file.extension}")


def split_file(file: str) -> File:
    """
    splits file to 4 parts
    e.g. File('c:\\work\\open_doc', 'summary', '4', 'ods')
    :returns: named tuple File with 4 arguments
    """
    counter: str = ''
    path, filename = os.path.split(file)
    filename, extension = os.path.splitext(filename)
    result: re.Match = counter_form.match(filename)
    if result:
        filename = result.group(1)
        counter = inc(result.group(2))
    return File(path, filename, counter, extension)


def inc(x: str) -> str:
    """ increments numeric string (NOTE: intentionally untreated ValueError) """
    return str(int(x) + 1)
