import ezodf
from ezodf import Table

from connect_lib.connector import Connector, CData


class ConnectOds(Connector):
    def create_sheet(self, data: CData) -> Table:
        """ vytvoří nový open office sheet, do kterého vloží získané hodnoty """
        sheet = ezodf.Table(self.sheet_name)
        sheet.reset(size=data.shape)
        self.put_data(sheet, data)  # worksheet is changed - added data
        return sheet

    def save_sheet(self, sheet: Table, output_file: str):
        """ saves sheet as workbook/spreadsheet file """
        spreadsheet = ezodf.newdoc(doctype="ods", filename=output_file)
        spreadsheet.sheets.append(sheet)
        spreadsheet.save()

    def get_sheet(self, file: str) -> Table:
        """ :return: opens xlsx or ods file and return its sheet / worksheet """
        ods = ezodf.opendoc(file)
        sheet = ods.sheets[0]
        return sheet

    def put_data(self, sheet, data: CData):
        """ put data to sheet, first header then rows with values """
        for i_col, name in enumerate(data.get_header()):
            sheet[0, i_col].set_value(name if name else '')
        for i_row, row in enumerate(data.get_rows(), start=1):
            for i_col, value in enumerate(row):
                sheet[i_row, i_col].set_value(value if value else '')
