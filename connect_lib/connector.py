import os
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Dict, List, Optional, Union, Tuple, Generator, Iterable

from ezodf import Table
from openpyxl.worksheet.worksheet import Worksheet


@dataclass
class CRow:
    file: str
    header: Dict[str, str] = field(default_factory=dict)
    row: Dict[str, str] = field(default_factory=dict)

    @property
    def path(self) -> str:
        return os.path.split(self.file)[0]

    @property
    def file_name(self) -> str:
        return os.path.split(self.file)[1]

    def add_cell(self, header_ref: str, header: str, value: str):
        """ adds cell value """
        self.header[header_ref] = header
        self.row[header_ref] = value

    def get_header(self):
        return self.header.items()

    def get_values(self, header_ref_order: Optional[List[str]] = None) -> Iterable:
        """ :returns: data in row """
        if header_ref_order:
            data: List[any] = []
            for ref in header_ref_order:
                data.append(self.row[ref])
            return data
        return self.row.values()


@dataclass
class CData:
    header_ref_order: List[str]
    rows: List[CRow] = field(default_factory=list)

    @property
    def shape(self) -> Tuple[int, int]:
        """
        :returns: number of rows and columns
        Note: '+2 columns' means 'path' and 'file name'
              '+1 rows' means header
        """
        columns = len(self.header_ref_order) + 2
        rows = len(self.rows) + 1
        return rows, columns

    def add_row(self, row: CRow):
        """ adds CRow to table list """
        self.rows.append(row)

    def get_header(self) -> List[str]:
        """
        :returns: connect header =put together names of cell through rows/files
        e.g. cell "C2" has name "Merchant" and another file "Merch" then result is "Merchant / Merch"
        """
        header: Dict[str, List[str]] = {ref: [] for ref in self.header_ref_order}
        for row in self.rows:
            for ref, name in row.get_header():
                if ref not in header:
                    header[ref] = [name]
                elif name not in header[ref]:
                    header[ref].append(name)
        return ['path', 'file name', *[' / '.join(names) for names in header.values()]]

    def get_rows(self) -> Generator:
        """ :returns: all rows """
        for row in self.rows:
            yield [row.path, row.file_name, *row.get_values(self.header_ref_order)]


class Connector(ABC):
    """ connector parent with defined obliged methods and collective methods """

    def __init__(self, sheet_name: Optional[str] = 'Summary'):
        self.sheet_name = sheet_name

    @abstractmethod
    def create_sheet(self, data: CData) -> Union[Worksheet, Table]:
        """ creates sheet and fill in data """
        pass

    @abstractmethod
    def save_sheet(self, sheet: Union[Worksheet, Table], output_file: str):
        """ saves sheet as workbook/spreadsheet file """
        pass

    @abstractmethod
    def get_sheet(self, file: str) -> Union[Worksheet, Table]:
        """ :return: opens xlsx or ods file and return its sheet / worksheet """
        pass

    @abstractmethod
    def put_data(self, sheet, data: CData):
        """ put collected data to sheet """
        pass

    def harvest_data(self, source_files: List[str], cells: Dict[str, str]) -> CData:
        """
        goes through all source_files (xlsx or ods) and get values from cells
        :return: collected data
        """
        data: CData = CData(header_ref_order=list(cells.keys()))
        for file in source_files:
            sheet = self.get_sheet(file)
            row: CRow = CRow(file)
            for header_ref, value_ref in cells.items():
                header = sheet[header_ref].value if sheet[header_ref].value else ''
                value = sheet[value_ref].value if sheet[value_ref].value else ''
                row.add_cell(header_ref, header, value)
            data.add_row(row)
        return data
