from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.worksheet.worksheet import Worksheet

from connect_lib.connector import Connector, CData


class ConnectXlsx(Connector):

    def create_sheet(self, data: CData) -> Workbook:
        """
        creates sheet and fill in data
        Note: Object references workbook-worksheet-data
        """
        workbook = Workbook()
        worksheet = workbook.active
        worksheet.title = self.sheet_name
        self.put_data(worksheet, data)  # worksheet is changed - added data
        return workbook

    def save_sheet(self, sheet: Workbook, output_file: str):
        """ saves sheet as workbook/spreadsheet file """
        sheet.save(output_file)

    def get_sheet(self, file: str) -> Worksheet:
        """ :return: opens xlsx or ods file and return its sheet / worksheet """
        workbook = load_workbook(filename=file)
        worksheet = workbook.active
        return worksheet

    def put_data(self, sheet: Worksheet, data: CData):
        """ Saves data to worksheet (sheet) """
        sheet.append(data.get_header())
        for row in data.get_rows():
            sheet.append(row)
