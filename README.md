# connect_ods_or_xls

## What the project is about?
Connects data from many excel files or spreasheet files to one.
These files must have same 'form' to get values and value names given by absolute position in table (e.g. 'F1').

## Install
no need to install just download project from gitlab

## Requirements
You need to install Python 3.7 and higher and install libraries of third side
```shell
pip install -r requirements.txt
```
Note: Code maybe run on lower versions of Python, but it was not tested.

## Example
```shell
python connect_ods_or_xls c:\\work\\excel_project\\ -o summary.xlsx -c cells.ini -t xlsx
```
1) Finds all excel files in folder `excel_project` and theirs subfolders.
2) Read information of all found files according to `cells.ini` instruction.

cells.ini file contains:
```ini
A1 A2
B1 B2
```
where `A1` and `B1` are absolute reference to cells with value names, `A2` and `B2` are absolute reference to cells with values 

3) Gained data writes to `summary.xlsx`

Names of columns are value names of cells `A1` and `B1`. Values of columns are data from cells `A2` and `B2` of various files.
```text
First Name | Second Name
------------------------
Peter      | Novak
Radek      | Kaspar
Josef      | Soukal
```
**Note:** Program expects that name of columns `First Name` and `Second Name` are same in all excel files.

## Help
For more information just use
```shell
python connect_ods_or_xls -h
```
