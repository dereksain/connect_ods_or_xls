import argparse
import csv
import os
from typing import List, Dict, Union

from ezodf import Table
from openpyxl import Workbook

from argument_lib.validate_args import create_argument_parser, validate_args, split_file, Args
from connect_lib.connect_ods import ConnectOds
from connect_lib.connect_xlsx import ConnectXlsx
from connect_lib.connector import Connector, CData


def main():
    """ according to input arguments collects data from xlsx or ods doc and saves them in one sheet """
    parser = create_argument_parser()
    args: argparse.Namespace = parser.parse_args()
    args: Args = validate_args(args)

    cells: Dict[str, str] = get_cells(args.cell_init_file)
    source_files: List[str] = get_source_files(args.source_path, args.output_file, args.type_of_file)

    connector: Connector = get_connector(args.type_of_file)
    data: CData = connector.harvest_data(source_files, cells)
    sheet: Union[Table, Workbook] = connector.create_sheet(data)
    connector.save_sheet(sheet, args.output_file)


def get_cells(file_cells: str) -> Dict[str, str]:
    """
    Načte ze souboru do slovníku jména buněk, která se mají importovat
    :return: klíč slovníku obsahuje odkaz na buňku s názvem hodnoty
             hodnota slovníku obsahuje odkaz na buňku s hodnotou
    """
    f = open(file_cells, "r")
    data = csv.reader(f, delimiter=' ')
    cells: Dict[str, str] = {}
    for d in data:
        cells[d[0]] = d[1]
    f.close()
    return cells


def get_source_files(source_path: str, output_file: str, extension: str) -> List[str]:
    """ Najde všechny ods soubory v adresáři, ze kterých se mají data importovat """
    source_files: list = []
    for file in get_files(source_path):
        filename = os.path.split(file)[1]
        if filename.endswith(extension) and split_file(output_file).filename not in filename:
            source_files.append(file)
    return source_files


def get_files(source_path: str) -> List[str]:
    """ najde všechny soubory v dané složce včetně podadresářů """
    all_files = []
    for root, dirs, files in os.walk(source_path):
        for name in files:
            all_files.append(os.path.join(root, name))
    return all_files


def get_top_level_folder_name(source_path: str) -> str:
    """ get name of top level folder """
    return os.path.normpath(source_path).split(os.sep)[-1]


def get_connector(extension: str) -> Connector:
    """ chooses right connector object according to file extension input argument """
    if extension == '.xlsx':
        connector = ConnectXlsx()
    elif extension == '.ods':
        connector = ConnectOds()
    else:
        raise ValueError(f'Unknown file extension / type of file: {extension}. Connector could not be assigned.')
    return connector


if __name__ == "__main__":
    main()
